I am applying for the MS in Computer Science course and looking forward to do
specialization in the Systems domain of the course.

Back in 2001, when I was in second grade, my father brought home a self assembled
Pentium 3 based PC alongside a then popular Digit magazine. I hardley knew then
that i would be spending a lot of my childhood besides that machine playing games
and looking up the internet on a dial-up modem for the meaning of stuff like GHz
and 386. But i can look onto that moment now as the starting point of a journey
in the realms of computer hardware and software.

At the time of choosing my courses to do in the college, my previous familiarity
with computers had a strong role in me deciding to pursue studies in Computer
Science. Also, when it came to actual decision making, i simply went through
the syllabus of the courses available to me and gone with the one i was most
familiar and interested in, Computer Science. The reason i chose software over
electronics was due to my belief that Software Enginering requires lesser
infrastructure and costs for doing projects, making the barier for entry really
shallow.

In my second semester of college, i came across a magazine in library that
contained a new operating system, Ubuntu 9.04. The words that caught my eyes were
'No Viruses, Free to use and Distribute'. To a guy that had known all his life that
Antivirus programs were a part of the system and that tweaking with windows registry
entries is the most customization he can do, this was intreaguing. I never ended
up using the OS but it introduced me to the concept of Open Source and linux.
The fact that, if i had the knowledge, i could modify and improve the OS was
really empowering. I see it now as the spark that lead me to win 3 programing
competitons, 4 best project awards and organizing 2 workshops on Linux and Python
in college. The high level of skill required to work in the lower level realms of
software like managing effective use of memory, cpu and peripherals, provoding
elegant interfaces for high level programs and scaling the programs to multiple
CPUs and machines convinced me of the fact that systems is where the real programming
is going on and this is the place where i want to spend my life.

This lead me to work on projects related to backend web API development and
scaling it horizontally to multiple machines. The concepts involved were directly
related to what we see in a distributed system. And the fact that i turned from
a software developer working in a company to a freelance software developer introduced
me to some really interesting projects. One of them was a project that involved
direct coding on a low level in C to program ARM based processor LPC2148, in
conjunction with an ARM based raspberry PI. Again, my limited knowledge proved
a hindrance in completing the project. It quickly dawned on me that
though i was using libraries to assist me in scaling applications and doing low
level stuff, my core knowledge on this domain was really limited.

This is the reason that i want to pursue a Masers degree in this domain, so that
i can imporove my knowledge of the systems domain and enhance my freelance
portfolio by doing more systems related projects. The reason that i am choosing
this college is because of the research undertaken here. (Enter info about research).
Learning from the expertise of faculty members like (enter names) is great way to
improve my skills in the field.
