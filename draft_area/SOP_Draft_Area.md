# IMPORTANT QUESTIONS TO CONSIDER FOR AN SOP

## What you want to study at graduate school?
    MS CS in Systems

## Why you want to study only this degree?
        We all know that programming in terms of Software is categorized in two
        levels, systems and application. I personaly like to categorize it into
        3 levels : high, medium and low. High level comprizes of writing application
        software using tools and infrastructure provided by a specific set of
        software, categorized under medium level. Medium level being software
        which provide services and infrastructure for high level applicaions to
        be made; and the low level being the assembly and machine code that is
        directly run on the CPU.
    
        It is this middle level software development, which comprizes of an OS
        and other libraries facilitating development of high level applications,
        is the area which i want to work on. Techinical challenges in this area
        are the ones that intreague me the most. For me,  solving challenges
        like efficient memory, CPU and peripheral utilization, providing elegant
        API interfaces for application development and scaling the software to
        multipe CPU, devices and cluster of computers is the best way to spend
        my time. Because of these reasons i have spent 28 months working on
        building backend infrastructure for web applications and scaling them to
        multiple machines comprising a distributed system. In the later part of
        my career, i had the opportunity to work on a low level project, which i
        absolutely loved. The project was programing the sensors for monitoring
        pollution emisison levels in small scale industries, managed via.
        Raspberry PI and providing web interface for monitoring these emission
        levels. As of now, this project is deployed in remote locations of 5
        different companies in states of Punjab and Maharshtra, India.


## Why do you want to study at this particular college? What do you like in us?
        Great course catalog and professors. (Find and compile info about the
        courses and professors )


## Why did you choose to study in this particular country? What do you like about it?
        USA has always been a hub for a multi nationl crowd coming in to study.
        This gives me an opportunity to study alongside some of the best minds
        of the world belonging to multipe nations. It gives diversity in thoughts
        and opinions which is a great recepie for excellent problem solving.


## How much and what kind of experience you have in your field?
        A total of 28 months of experience. Done 1 project related to systems
        (Pollution monitoring tool, programing embedded device for monitoring
        pollution) and many web based projcts involving the concepts of
        distributed systems.


## Is your experience related to you choice of degree?
        Yes. One project directly related and other projects involving
        concepts of Distributed Systems


## If you are already experienced, what additional skills are you planning to gain from the degree?

## What you plan to do with your degree after graduation?
        I plan to use the skills learned to expand my freelance portfolio to
        work on Systems related projects, especially in Distributed Cloud
        Environments


## Would you choose to end up with a job or take up research?
        I would prefer a job


## What are your expectations from both the graduate program, and the university?

## Would you like to study or do research under any particular professor? If yes, why only them?

## How can you contribute to our university and our program?

## What specific skills do you bring to the table?
        Strong programming skills, risk taking (freelance and own company)


## Apart from work and education, what are your hobbies, interests, and habits?

## What are you like, as a person?

## What do you understand about our student community and culture? Why do you think you will fit in?

## What is that one unique aspect/characteristic about you that we should know? Why does it matter to us or to the fellow students of your class?


# Tips / Strategies

## Write Stories. Not Statements.
        Think again. Do you want your statement of purpose to read like a novel
        or a newspaper? If the former is your answer, then you need to put in a
        lot of effort to tell your story. Think about ‘why’ you want to study
        what you want to study. Is there a strong reason behind it?Is the reason
        emotional, economical, or any other? Think hard, and you will find a
        connection. The reason might not seem obvious in plain sight, but when
        you think hard enough, you will understand that there is strong reason
        why you want to study a particular course/degree. Now, when you have found
        this strong reason, tell it as a story. Write a short, but great narrative
        about what made you make this choice. About why you have chosen to study
        this course at this university.


## Quantify Your Stories
        Your story must contain measurable quantities instead of just stories,
        so the reader can understand the depth of it. These numbers suddenly
        give a whole new perspective to the readers, and their respect for you
        is suddenly multiplied. That’s the power of numbers; they add authenticity,
        and authority to your stories. If you can quantify your stories properly,
        and show the results instead of just actions, the committee will not forget
        your name.


## Be Specific
        Don’t just say something because you think it will impress the admissions
        committee. Whatever you say, you have to really dig into details.


## Customize Your Essay 
        Different for each application, because each university has a diverse set
        of characteristics that define them, and their cultures, methodologies,
        visions, values, mottos, strengths, weaknesses, etc., vary greatly. 


## Use a Formal But Conversational Tone 

## Decide How You Want To Portray Yourself. And Learn How to Portray Indirectly.
        learn what your statement of purpose should portray you as, in terms of
        a few criteria, which tell the admissions committee that you are:

        - Very passionate about the field of study you have chosen.
        - An Intelligent student who can withstand the academic workload of a 
        graduate program
        - Well-prepared academically and personally, and eager to study new courses.
        - Able to take on the challenges of studying at an international graduate
        school.
        - Able to build and maintain a good rapport with professors and fellow
        grad students.
        - Able to finish the graduate degree within time, and graduate with a 
        good percentage.
        - A potential remarkable representative of that grad school in your
        future career.
        - A successful alumni of the grad school who in the future can help in
        recruiting graduates.
        - A responsible alumni who in the future will help raise funds for the 
        grad school, to spend on research, infrastructure, facilities, student
        scholarships, etc.


## Don’t Create Stories. Be Yourself 

## Address Your Problems 

## Do Your Homework 

## Do Your Homework
    Specifics are really important


## Proofread, Edit, and Re-edit. Ask Friends and Family To Grade Your Essay

## Ask Advice From Professors. 



# Checklist

- Introducing yourself in a unique manner.
- Demonstrating your passion for the field.
- Story about your background or experience in the field you’ve chosen.
- Description of your academic background in the field you’ve chosen.
- Specific classes or special courses you have taken, that are related to your field of interest.
- Some of the professors you have studied under, especially if they are well-known in that field.
- Co-curricular and Extracurricular activities in the field of you interest.
- Publications or other professional accomplishments in the field (perhaps conference presentations or public readings)
- Any community service or leadership experience while in college.
- Explanations about problems in background (if needed)
- Explanation of why you have chosen the specific grad school and other related questions as discussed in the beginning of this article.
- Mention what you like about the university you are applying for, and why: facilities, infrastructure, etc.
- Mention names of one or two professors in that school and what you know of and appreciate about their work, and why you want to study or work under their guidance.
- Specific features of the grad program and the university, which attract you personally. And why.
- Get advice from several of your professors, family, and close friends. Ask for stories about yourself.
- Proofread and edit; ask friends and family to proofread for you as well.


# Importnat ponuts to consider, by Nipun
- Why you are interested in the program
- How your background will contribute to your success in the program
- Briefly describe your career goals.
